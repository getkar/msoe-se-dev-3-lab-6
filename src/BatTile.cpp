//============================================================================
// Name        : BatTile.cpp
// Author      : Robert Getka
// Version     : SE 2040, Spring 2016
//============================================================================
#include "Tile.h"

BatTile::BatTile() {
    notifyMessage0 = "You are on a Bat Tile";
    notifyMessage1 = "You are one space away from a Bat Tile";
}

int BatTile::getType() {
    return 0;
}

char BatTile::getPrintChar() {
    return 'B';
}






