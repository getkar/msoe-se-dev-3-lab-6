//============================================================================
// Name        : Application.cpp
// Author      : Blake Stacks
// Version     : SE 2040, Spring 2016
//============================================================================
#include <iostream>
#include <map>
#include <algorithm>

#include "Application.h"
#include "GameBoard.h"

using namespace std;

int Application::state = PLAYING;

/**
 * Starts the application. Creates the command map, runs the input loop,
 * manages different endgames (quit, win, lose).
 */
void Application::start() {
    map<string, int> instructions{{"n",     GameBoard::NORTH},
                                  {"s",     GameBoard::SOUTH},
                                  {"e",     GameBoard::EAST},
                                  {"w",     GameBoard::WEST},
                                  {"an",    GameBoard::ARROW_NORTH},
                                  {"as",    GameBoard::ARROW_SOUTH},
                                  {"ae",    GameBoard::ARROW_EAST},
                                  {"aw",    GameBoard::ARROW_WEST},
                                  {"q",     GameBoard::QUIT},
                                  {"u",     GameBoard::STAIRS_UP},
                                  {"d",     GameBoard::STAIRS_DOWN},
                                  {"debug", GameBoard::DEBUG_OUT}};
    printMessage("You started a new game.");
    GameBoard *board;
    if (debugMode) {
        board = new GameBoard(0);
    } else {
        board = new GameBoard();
    }
    printMessage(
            "Action: N)orth, S)outh, E)ast, W)est, shoot Arrow (an=north, as=south, ae=east, aw=west), Q)uit, debug) display map");
    string next_line;
    getline(cin, next_line);  // read input up to end-of-line
    while (cin && state == PLAYING) {
        transform(next_line.begin(), next_line.end(), next_line.begin(),
                  ::tolower);
        if (next_line.size() > 0 &&
            instructions.find(next_line) != instructions.end()) {
            if (instructions[next_line] == GameBoard::QUIT) {
                printMessage("Player quit the game.");
                break;
            }
            board->doMove(instructions[next_line]);
            next_line = "";
        } else {
            printMessage(
                    "Action: N)orth, S)outh, E)ast, W)est, shoot Arrow (an=north, as=south, ae=east, aw=west), Q)uit, debug) display map");
            getline(cin, next_line);
        }
    }
    if (state == LOSE) {
        printMessage("You died. Game over.");
    } else if (state == WIN) {
        printMessage("You have found and killed the monster. You win!");
    }
    delete board;
    cout << "Press any key to continue...";
    cin.clear();
    cin.ignore();
    cin.get();
}

/**
 * Outputs the given message to a new line in the console
 */
void Application::printMessage(string message) {
    if (message.length() > 0) {
        cout << message << endl;
    }
}

/**
 * sets the game state to the given state
 */
void Application::setState(int s) {
    state = s;
}

/**
 * Default constructor. Initializes the state
 */
Application::Application() {
    state = PLAYING;
}

Application::Application(int) {
    printMessage("::::::::debug mode::::::::");
    debugMode = true;
}