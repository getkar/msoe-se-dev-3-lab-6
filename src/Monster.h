//============================================================================
// Name        : Monster.h
// Author      : Trevor Hacker
// Version     : SE 2040, Spring 2016
//============================================================================

#ifndef monster_h
#define monster_h
#include "Entity.h"
#include "Application.h"

//stores a Monster object as an entity
class Monster : public Entity {
public:

	//default constructor
	Monster();

	//constructor that takes in parameters for the position of the entity
	//and sets the position based on x, y coordinates and which level
	//it is on
	Monster(int x, int y, int level);

	//prints a '!' char
	char getPrintChar();

	//notifies Application to print a message
	//if the player is one tile away,
	//notifies: 'you smell something bad'
	//if player lands on tile, 
	//notifies: 'you were eaten by a monster'
	void notify(int);
};
#endif