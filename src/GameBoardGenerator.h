//============================================================================
// Name        : GameBoardGeneratior.h
// Author      : Robert Getka
// Version     : SE 2040, Spring 2016
//============================================================================

#ifndef PROJECT_GAMEBOARDGENERATOR_H
#define PROJECT_GAMEBOARDGENERATOR_H
#include "GameBoard.h"

//A class that generates the game board
class GameBoardGenerator{
public:
    //constructor that uses the current system time as a random seed
    GameBoardGenerator(GameBoard *gameBoard);
    //constructor that uses the provided random seed
    GameBoardGenerator(GameBoard *gameBoard, unsigned int seed);
    // generates the game board
    void generate();
    //destructor
    virtual ~GameBoardGenerator();

    //the maximum amount of arrows allowed to be generated
    static const int MAXARROW = 7;
private:
    //the maximum amount of stairs allowed to be generated
    const int MAXSTAIR = 2;
    //the GameBoard to be generated
    GameBoard *gameBoard;

    // generates all of the tiles on the gameboard
    void generateTiles();
    // generates a specific tile located at x,y,l
    void generateTile(int x, int y,int l);
    // generates all of the entities on the gameboard
    void generateEntities();
    //generates the stairs on the gameboard
    void generateStairs();

};
#endif //PROJECT_GAMEBOARDGENERATOR_H
