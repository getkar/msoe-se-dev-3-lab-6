//============================================================================
// Name        : Application.h
// Author      : Blake Stacks
// Version     : SE 2040, Spring 2016
//============================================================================

#ifndef LAB6_APPLICATION_H
#define LAB6_APPLICATION_H

#include <string>

/**
 * The main application class. Manages all console input and
 * output and the overall game state (win, lose, playing)
 */
class Application {
public:
    void start();

    static void printMessage(std::string);

    static void setState(int);

    Application();

    Application(int);

    //state constants
    constexpr static int WIN = 1, PLAYING = 0, LOSE = 2;
private:
    static int state;
    bool debugMode = false;
};


#endif //LAB6_APPLICATION_H
