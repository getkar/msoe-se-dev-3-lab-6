//============================================================================
// Name        : StairTile.cpp
// Author      : Robert Getka
// Version     : SE 2040, Spring 2016
//============================================================================
#include "Tile.h"
StairTile::StairTile() {
    notifyMessage0 = "";
    notifyMessage1 = "You are one space away from a stair tile";
}

int StairTile::getType() {
    return 3;
}

char StairTile::getPrintChar() {
    return 'S';
}


