//============================================================================
// Name        : Player.h
// Author      : Trevor Hacker
// Version     : SE 2040, Spring 2016
//============================================================================

#ifndef player_h
#define player_h
#include "Entity.h"

//stores a Player object
class Player: public Entity{
public:

	//default constructor
	Player();

	//constructor that takes in parameters for the position of the entity
	//and sets the position based on x, y coordinates and which level
	//it is on
	Player(int x, int y, int level);

	//prints a '+' char
	char getPrintChar();

	//does nothing
	void notify(int);
};
#endif