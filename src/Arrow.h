//============================================================================
// Name        : Arrow.h
// Author      : Trevor Hacker
// Version     : SE 2040, Spring 2016
//============================================================================

#ifndef arrow_h
#define arrow_h
#include "Entity.h"

//stores an Arrow object
class Arrow : public Entity {
public:

	//default constructor
	Arrow();

	//constructor that takes in parameters for the position of the entity
	//and sets the position based on x, y coordinates and which level
	//it is on
	Arrow(int x, int y, int level);

	//prints a '-' char
	char getPrintChar();

	//notifies Application to print a message
	//if player lands on tile, 
	//notifies: 'you picked up an arrow'
	void notify(int);
};
#endif
