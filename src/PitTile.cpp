//============================================================================
// Name        : PitTile.cpp
// Author      : Robert Getka
// Version     : SE 2040, Spring 2016
//============================================================================
#include "Tile.h"
PitTile::PitTile() {
    notifyMessage0 = "";
    notifyMessage1 = "You are one space away from a Pit";
}

int PitTile::getType() {
    return 2;
}

char PitTile::getPrintChar() {
    return '@';
}



