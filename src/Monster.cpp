//============================================================================
// Name        : Monster.cpp
// Author      : Trevor Hacker
// Version     : SE 2040, Spring 2016
//============================================================================

#include "Monster.h"

Monster::Monster() {}

Monster::Monster(int _x, int _y, int _level) :Entity(_x, _y, _level){}

char Monster::getPrintChar() {
	return '!';
}

void Monster::notify(int distance){
	if (distance == 0) {
		Application::printMessage("You were eaten by a monster");
	}
	if (distance == 1) {
		Application::printMessage("You smell something bad");
	}
}