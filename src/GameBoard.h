//============================================================================
// Name        : GameBoard.h
// Author      : Blake Stacks
// Version     : SE 2040, Spring 2016
//============================================================================

#ifndef LAB6_GAMEBOARD_H
#define LAB6_GAMEBOARD_H


#include "Tile.h"
#include "Entity.h"

/**
 * The Gameboard logic class. Manages the gameboard, all player-tile
 * and player-entity interactions, and player movement
 */
class GameBoard {
public:
    GameBoard(int);

    //commmand constants
    static const int NORTH, SOUTH, EAST, WEST, ARROW, ARROW_NORTH, ARROW_SOUTH, ARROW_EAST, ARROW_WEST,
            QUIT, STAIRS_UP, STAIRS_DOWN, DEBUG_OUT;
    //level constants
    static const int SQUARE_SIZE, LEVELS;

    void doMove(int);

    GameBoard();

    virtual ~GameBoard();

    Tile *getTile(int, int, int);

    void setTile(int, int, int, Tile *);

    void setEntity(Entity *e);

    void deleteArrow(int index);

    void arrowMissed(Entity *monster);


private:
    Tile **board;
    Entity **entities;
    int numEntities = 0;
    int numArrows = 3;
    bool monsterHit=false;

    int getSafePosition(int, int, int);


    void debugPrint();

    int getEntity(int x, int y, int l);

    void printEntities();

    int getDist(Entity *a, Entity *b);

    void printTiles();

    void moveMonster(Entity *monster);

    void doMonsterMove(Entity *monster);
};


#endif //LAB6_GAMEBOARD_H
