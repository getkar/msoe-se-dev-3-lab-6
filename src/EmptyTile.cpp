//============================================================================
// Name        : EmptyTile.cpp
// Author      : Robert Getka
// Version     : SE 2040, Spring 2016
//============================================================================
#include "Tile.h"

EmptyTile::EmptyTile() {
    notifyMessage0 = "";
    notifyMessage1 = "";
}

int EmptyTile::getType() {
    return 1;
}


char EmptyTile::getPrintChar() {
    return '.';
}



