//============================================================================
// Name        : Entity.h
// Author      : Trevor Hacker
// Version     : SE 2040, Spring 2016
//============================================================================

#ifndef entity_h
#define entity_h
#include "Application.h"

//stores an entity object that is placed on the tile
class Entity {
public:

	//default constructor 
	Entity();

	//constructor that takes in parameters for the position of the entity
	//and sets the position based on x, y coordinates and which level
	//it is on
	Entity(int x, int y, int level);

	//sets the position based on x, y coordinates and which level
	//it is on
	void setPosition(int x, int y, int level);

	//retuns the x position of the entity
	int getX();

	//returns the y position of the entity
	int getY();

	//returns the level the entity is on
	int getLevel();

	//prints a char related to the specific entity
	virtual char getPrintChar() = 0;

	//notifies the Application to 
	//print a message for each entity depending on
	//the distance from the player
	virtual void notify(int) = 0;

private:
	bool isAlive;
	int x, y, level;
};
#endif