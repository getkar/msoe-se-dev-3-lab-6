//============================================================================
// Name        : GameBoardGenerator.cpp
// Author      : Robert Getka
// Version     : SE 2040, Spring 2016
//============================================================================
#include "GameBoardGenerator.h"
#include "ctime"
#include "Player.h"
#include "Monster.h"
#include "Arrow.h"

GameBoardGenerator::GameBoardGenerator(GameBoard *gameBoardin) {
    gameBoard = gameBoardin;
    std::srand(std::time(0));
}

GameBoardGenerator::GameBoardGenerator(GameBoard *gameBoardin, unsigned int seed) {
    gameBoard = gameBoardin;
    std::srand(seed);
}

void GameBoardGenerator::generate() {
    generateStairs();
    generateTiles();
    generateEntities();
}

void GameBoardGenerator::generateStairs() {
    for (int j = 0; j < MAXSTAIR; j++) {
        if (j == 0 || rand() % 2 == 0) {
            int x = rand() % GameBoard::SQUARE_SIZE, y =
                    rand() % GameBoard::SQUARE_SIZE;
            for (int i = 0; i < GameBoard::LEVELS; ++i) {
                gameBoard->setTile(x, y, i, new StairTile());
            }
        }
    }
}

void GameBoardGenerator::generateTiles() {

    for (int x = 0; x < GameBoard::SQUARE_SIZE; ++x) {
        for (int y = 0; y < GameBoard::SQUARE_SIZE; ++y) {
            for (int l = 0; l < GameBoard::LEVELS; ++l) {
                if ((*(*gameBoard).getTile(x, y, l)).getType() == 1) {
                    if((*gameBoard->getTile(x, y, l)).getType() != 3)
                        generateTile(x, y, l);
                }
            }
        }
    }
}

void GameBoardGenerator::generateTile(int x, int y, int l) {
    int emptyTile = 80;
    int batTile = 10;
    int pitTile = 10;

    int r = rand() % 100;

    if(r <= emptyTile){
        gameBoard->setTile(x, y, l, new EmptyTile());
    } else if(r <= batTile+emptyTile){
        gameBoard->setTile(x, y, l, new BatTile());
    }else if(r <= batTile+emptyTile+pitTile){
        gameBoard->setTile(x, y, l, new PitTile());
    }
}

GameBoardGenerator::~GameBoardGenerator() {

}

void GameBoardGenerator::generateEntities() {

    int px = rand() % GameBoard::SQUARE_SIZE;
    int py = rand() % GameBoard::SQUARE_SIZE;
    int pl = rand() % GameBoard::LEVELS;

    Tile *pTile = gameBoard->getTile(px,py,pl);
    while(pTile->getType()!= 1){
        px = rand() % GameBoard::SQUARE_SIZE;
        py = rand() % GameBoard::SQUARE_SIZE;
        pl = rand() % GameBoard::LEVELS;
        pTile = gameBoard->getTile(px,py,pl);
    }
    Player *player = new Player(px,py,pl);


    int mx = rand() % GameBoard::SQUARE_SIZE;
    int my = rand() % GameBoard::SQUARE_SIZE;
    int ml = rand() % GameBoard::LEVELS;
    Tile *mTile = gameBoard->getTile(mx,my,ml);
    while(mTile->getType()!= 1||pTile==mTile) {
        mx = rand() % GameBoard::SQUARE_SIZE;
        my = rand() % GameBoard::SQUARE_SIZE;
        ml = rand() % GameBoard::LEVELS;
        mTile = gameBoard->getTile(mx,my,ml);
    }
    Monster *monster = new Monster(mx,my,ml);

    gameBoard->setEntity(player);
    gameBoard->setEntity(monster);

    for (int j = 0; j < MAXARROW; j++) {
        int ax = rand() % GameBoard::SQUARE_SIZE;
        int ay = rand() % GameBoard::SQUARE_SIZE;
        int al = rand() % GameBoard::LEVELS;

        Tile *aTile = gameBoard->getTile(ax,ay,al);

        while(aTile->getType()!= 1||aTile==mTile||aTile==pTile) {
            ax = rand() % GameBoard::SQUARE_SIZE;
            ay = rand() % GameBoard::SQUARE_SIZE;
            al = rand() % GameBoard::LEVELS;
            aTile = gameBoard->getTile(ax,ay,al);
        }
        Arrow *arrow = new Arrow(ax,ay,al);
        gameBoard->setEntity(arrow);
    }

}

