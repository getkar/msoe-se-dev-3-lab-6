//============================================================================
// Name        : main.cpp
// Author      : Blake Stacks
// Version     : SE 2040, Spring 2016
//============================================================================
#include <iostream>
#include "Application.h"

using namespace std;

/**
 * The main method of the program. Starts the application.
 */
int main(int argc, char *argv[]) {
    if (argc == 1) {
        Application().start();
    } else if (argv[1][0] == 'd') {
        Application(0).start();
    }
}
