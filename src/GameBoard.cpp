//============================================================================
// Name        : GameBoard.cpp
// Author      : Blake Stacks
// Version     : SE 2040, Spring 2016
//============================================================================
#include <string>
#include "GameBoard.h"
#include "GameBoardGenerator.h"

using namespace std;


const int GameBoard::NORTH = 0, GameBoard::SOUTH = 1, GameBoard::EAST = 2,
        GameBoard::WEST = 3, GameBoard::ARROW_NORTH = 4, GameBoard::ARROW_SOUTH = 9,
        GameBoard::ARROW_EAST = 10, GameBoard::ARROW_WEST = 11, GameBoard::QUIT = 5,
        GameBoard::STAIRS_UP = 6, GameBoard::STAIRS_DOWN = 7, GameBoard::DEBUG_OUT = 8;
const int GameBoard::SQUARE_SIZE = 5, GameBoard::LEVELS = 4;

/**
 * The main logic function for doing a single move. Manages player
 * movement, tile interaction, and entity interaction.
 */
void GameBoard::doMove(int move) {
    //pre-move
    string wallMsg = "You hit a wall.";
    Entity &player = *entities[0];
    Entity &monster = *entities[1];
    int dx = 0, dy = 0, dl = 0;

    //@move
    if (move == NORTH) {
        if (player.getY() == 0) {
            Application::printMessage(wallMsg);
        } else {
            dy = -1;
        }
    } else if (move == SOUTH) {
        if (player.getY() == SQUARE_SIZE - 1) {
            Application::printMessage(wallMsg);
        } else {
            dy = 1;
        }

    } else if (move == EAST) {
        if (player.getX() == SQUARE_SIZE - 1) {
            Application::printMessage(wallMsg);
        } else {
            dx = 1;
        }
    } else if (move == WEST) {
        if (player.getX() == 0) {
            Application::printMessage(wallMsg);
        } else {
            dx = -1;
        }
    } else if (move == ARROW_NORTH) {
        int arrowDist = rand() % 3 + 1;
        if (player.getX() == monster.getX() &&
            player.getLevel() == monster.getLevel() &&
            player.getY() > monster.getY() &&
            player.getY() - monster.getY() <= arrowDist) {
            if (monsterHit) {
                Application::setState(Application::WIN);
                return;
            } else {
                monsterHit = true;
                Application::printMessage(
                        "You shot an arrow at the monster and it hit! It looks badly injured.");
                moveMonster(&monster);
            }
        } else {
            arrowMissed(&monster);
        }
    } else if (move == ARROW_SOUTH) {
        int arrowDist = rand() % 3 + 1;
        if (player.getX() == monster.getX() &&
            player.getLevel() == monster.getLevel() &&
            player.getY() < monster.getY() &&
            monster.getY() - player.getY() <= arrowDist) {
            if (monsterHit) {
                Application::setState(Application::WIN);
                return;
            } else {
                monsterHit = true;
                Application::printMessage(
                        "You shot an arrow at the monster and it hit! It looks badly injured.");
                moveMonster(&monster);
            }
        } else {
            arrowMissed(&monster);
        }
    } else if (move == ARROW_EAST) {
        int arrowDist = rand() % 3 + 1;
        if (player.getY() == monster.getY() &&
            player.getLevel() == monster.getLevel() &&
            player.getX() < monster.getX() &&
            monster.getX() - player.getX() <= arrowDist) {
            if (monsterHit) {
                Application::setState(Application::WIN);
                return;
            } else {
                monsterHit = true;
                Application::printMessage(
                        "You shot an arrow at the monster and it hit! It looks badly injured.");
                moveMonster(&monster);
            }
        } else {
            arrowMissed(&monster);
        }
    } else if (move == ARROW_WEST) {
        int arrowDist = rand() % 3 + 1;
        if (player.getY() == monster.getY() &&
            player.getLevel() == monster.getLevel() &&
            player.getX() > monster.getX() &&
            player.getX() - monster.getX() <= arrowDist) {
            if (monsterHit) {
                Application::setState(Application::WIN);
                return;
            } else {
                monsterHit = true;
                Application::printMessage(
                        "You shot an arrow at the monster and it hit! It looks badly injured.");
                moveMonster(&monster);
            }
        } else {
            arrowMissed(&monster);
        }
    } else if (move == STAIRS_DOWN) {
        if (getTile(player.getX(), player.getY(),
                    player.getLevel())->getType() == StairTile().getType() &&
            player.getLevel() > 0) {
            dl = -1;
        }
    } else if (move == STAIRS_UP) {
        if (getTile(player.getX(), player.getY(),
                    player.getLevel())->getType() == StairTile().getType() &&
            player.getLevel() < LEVELS - 1) {
            dl = 1;
        }
    } else if (move == DEBUG_OUT) {
        debugPrint();
    }

    //post-move
    bool stillMoving;
    do {
        stillMoving = false;
        player.setPosition(player.getX() + dx, player.getY() + dy,
                           player.getLevel() + dl);
        int entity = getEntity(player.getX(), player.getY(),
                               player.getLevel());
        //start Trevor's code
        if (entity == 1) {
            Application::setState(Application::LOSE);
        }
        else if (entity > 1) {
            numArrows++;
            entities[entity]->notify(0);
            deleteArrow(entity);
        }
        //end trevor's code

        dl = 0;
        dx = 0;
        dy = 0;
        Tile &t = *getTile(player.getX(), player.getY(),
                           player.getLevel());
        if (t.getType() == PitTile().getType()) {
            if (player.getLevel() == 0) {
                Application::setState(Application::LOSE);
            } else {
                dl = -1;
                dx = rand() % (GameBoard::SQUARE_SIZE) - player.getX();
                dy = rand() % (GameBoard::SQUARE_SIZE) - player.getY();
                stillMoving = true;
                Application::printMessage(
                        "You fell down a pit to the floor below you.");
            }
        } else if (t.getType() == BatTile().getType()) {
            dx = rand() % (GameBoard::SQUARE_SIZE) - player.getX();
            dy = rand() % (GameBoard::SQUARE_SIZE) - player.getY();
            dl = rand() % (GameBoard::LEVELS) - player.getLevel();
            Application::printMessage(
                    "You were picked up by a bat and moved to a different location.");
            stillMoving = true;
        } else if (t.getType() == StairTile().getType()) {
            string msg = "You have stepped on a stair tile. In addition to normal moves, you can also move ";
            if (player.getLevel() < LEVELS - 1) {
                msg += "U)p";
            }
            if (player.getLevel() < LEVELS - 1 && player.getLevel() > 0) {
                msg += " or ";
            }
            if (player.getLevel() > 0) {
                msg += "D)own";
            }
            Application::printMessage(msg);
        }
        if (getDist(&player, &monster) == 0) {
            Application::setState(Application::LOSE);
        }
    } while (stillMoving);
//    debugPrint();
    printEntities();
    printTiles();
}

/**
 * Default constructor. initializes the board and enitity arrays
 * to be single dimensional dynamically allocated arrays of pointers
 * to their respective abstract superclasses. Populates board with
 * emptytiles then uses the gameboardgenerator to populate the board
 * with entities and different tiles
 */
GameBoard::GameBoard() {
    board = new Tile *[SQUARE_SIZE * SQUARE_SIZE * LEVELS];
    entities = new Entity *[GameBoardGenerator::MAXARROW + 2];
    for (int x = 0; x < SQUARE_SIZE; ++x) {
        for (int y = 0; y < SQUARE_SIZE; ++y) {
            for (int l = 0; l < LEVELS; ++l) {
                board[getSafePosition(x, y, l)] = new EmptyTile();
            }
        }
    }
    GameBoardGenerator(this).generate();
}

GameBoard::GameBoard(int) {
    board = new Tile *[SQUARE_SIZE * SQUARE_SIZE * LEVELS];
    entities = new Entity *[GameBoardGenerator::MAXARROW + 2];
    for (int x = 0; x < SQUARE_SIZE; ++x) {
        for (int y = 0; y < SQUARE_SIZE; ++y) {
            for (int l = 0; l < LEVELS; ++l) {
                board[getSafePosition(x, y, l)] = new EmptyTile();
            }
        }
    }
    GameBoardGenerator(this, 0).generate();
}

/**
 * gets a tile at the given location
 */
Tile *GameBoard::getTile(int x, int y, int l) {
    return board[getSafePosition(x, y, l)];
}

/**
 * sets the tile at the given position
 */
void GameBoard::setTile(int x, int y, int l, Tile *t) {
    board[getSafePosition(x, y, l)] = t;
}

/**
 * default destructor, deletes each pointer in each dynamically allocated
 * array, then the the pointer arrays themselves
 */
GameBoard::~GameBoard() {
    for (int i = 0; i < SQUARE_SIZE * SQUARE_SIZE * LEVELS; ++i) {
        delete board[i];
    }
    for (int i = 0; i < numEntities; ++i) {
        delete entities[i];
    }
    delete[] board;
    delete[] entities;
    delete board;
    delete entities;
}

/**
 * gets the entity at the given location, -1 if not found
 */
int GameBoard::getEntity(int x, int y, int l) {
    for (int i = 1; i < numEntities; ++i) {
        Entity &e = *entities[i];
        if (e.getX() == x && e.getY() == y && e.getLevel() == l) {
            return i;
        }
    }
    return -1;
}

/**
 * deletes an arrow from the entity array
 */
void GameBoard::deleteArrow(int index) {
    if (index < 2) {
        throw "Cannot delete the Player or Monster";
    }
    delete entities[index];
    for (int i = index + 1; i <= numEntities; i++) {
        entities[i - 1] = entities[i];
    }
    numEntities--;
}

/**
 * Prints each level to the console
 */
void GameBoard::debugPrint() {
    char charBoard[SQUARE_SIZE * SQUARE_SIZE * LEVELS];
    for (int x = 0; x < SQUARE_SIZE; ++x) {
        for (int y = 0; y < SQUARE_SIZE; ++y) {
            for (int l = 0; l < LEVELS; ++l) {
                charBoard[getSafePosition(x, y, l)] =
                        getTile(x, y, l)->getPrintChar();
            }
        }
    }
    for (int i = 0; i < numEntities; ++i) {
        int x = entities[i]->getX(), y = entities[i]->getY(), l = entities[i]->getLevel();
        charBoard[getSafePosition(x, y, l)] = entities[i]->getPrintChar();
    }
    for (int l = LEVELS - 1; l >= 0; --l) {
        Application::printMessage("Level " + to_string(l + 1));
        for (int y = 0; y < SQUARE_SIZE; ++y) {
            string line = "";
            for (int x = 0; x < SQUARE_SIZE; ++x) {
                line += " ";
                line += charBoard[getSafePosition(x, y, l)];
            }
            Application::printMessage(line);
        }
    }
}

/**
 * Gets the safe location in the 1D board array given an x, y, and level
 */
int GameBoard::getSafePosition(int x, int y, int l) {
    return x + y * SQUARE_SIZE + l * SQUARE_SIZE * SQUARE_SIZE;
}

/**
 * adds the entity to the entity list
 */
void GameBoard::setEntity(Entity *e) {
    entities[numEntities++] = e;
}

/**
 * prints all entity messages to the console (e.g. "you are 1 tile away from an arrow")
 */
void GameBoard::printEntities() {
    Entity &player = *entities[0];
    for (int i = 0; i < numEntities; ++i) {
        entities[i]->notify(getDist(&player, entities[i]));
    }
}

/**
 * prints all tile messages to the console (e.g. "you are on a stair tile")
 */
void GameBoard::printTiles() {
    Entity &player = *entities[0];
    for (int x = -1; x <= 1; ++x) {
        if (!(player.getX() + x < 0 || player.getX() + x >= SQUARE_SIZE)) {
            for (int y = -1; y <= 1; ++y) {
                if (!(player.getY() + y < 0 ||
                      player.getY() + y >= SQUARE_SIZE)) {
                    board[getSafePosition(player.getX() + x, player.getY() + y,
                                          player.getLevel())]->notify(
                            (x == 0 && y == 0) ? 0 : 1);
                }
            }
        }
    }
}

/**
 * gets the distance of entity a to entity b, 0 if at same
 * location, 1 if 1 tile away (8 planar locations), -1 if >1 tile away
 */
int GameBoard::getDist(Entity *a, Entity *b) {
    auto x = a->getX() - b->getX(), y = a->getY() - b->getY(),
            l = a->getLevel() - b->getLevel();
    if (x == 0 && y == 0 && l == 0) {
        return 0;
    } else if ((x < 2 && x > -2) && (y < 2 && y > -2) && l == 0) {
        return 1;
    } else {
        return -1;
    }
}

/**
 * decreases the number of arrows
 */
void GameBoard::arrowMissed(Entity *monster) {
    numArrows--;
    string msg = "You missed! The monster moved and you now have " +
                 to_string(numArrows);
    if (numArrows != 1) {
        msg += " arrows.";
    } else {
        msg += " arrow.";
    }
    Application::printMessage(msg);
    doMonsterMove(monster);
}

/**
 * moves the monster to a random empty tile and decreases the number of arrows
 */
void GameBoard::moveMonster(Entity *monster) {
    numArrows--;
    string msg = "The monster moved and you now have " +
                 to_string(numArrows);
    if (numArrows != 1) {
        msg += " arrows.";
    } else {
        msg += " arrow.";
    }
    Application::printMessage(msg);
    doMonsterMove(monster);
}


/**
 * moves the monster to a random empty tile and decreases the number of arrows
 */
void GameBoard::doMonsterMove(Entity *monster) {
    int x = rand() % (GameBoard::SQUARE_SIZE),
            y = rand() % (GameBoard::SQUARE_SIZE),
            l = rand() % (GameBoard::LEVELS);
    while (getTile(x, y, l)->getType() != EmptyTile().getType() &&
           getEntity(x, y, l) == -1) {
        x = rand() % (GameBoard::SQUARE_SIZE);
        y = rand() % (GameBoard::SQUARE_SIZE);
        l = rand() % (GameBoard::LEVELS);
    }
    monster->setPosition(x, y, l);
}




