//============================================================================
// Name        : Arrow.cpp
// Author      : Trevor Hacker
// Version     : SE 2040, Spring 2016
//============================================================================

#include "Arrow.h"

Arrow::Arrow() {}

Arrow::Arrow(int _x, int _y, int _level) :Entity(_x, _y, _level) {}

char Arrow::getPrintChar() {
	return '-';
}

void Arrow::notify(int distance) {
	if (distance == 0) {
		Application::printMessage("You picked up an arrow");
	}
}
