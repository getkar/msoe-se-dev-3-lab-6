//============================================================================
// Name        : Entity.cpp
// Author      : Trevor Hacker
// Version     : SE 2040, Spring 2016
//============================================================================

#include "Entity.h"

Entity::Entity() { }

Entity::Entity(int _x, int _y, int _level)
        : x(_x), y(_y), level(_level) { }

void Entity::setPosition(int _x, int _y, int _level) {
    x = _x;
    y = _y;
    level = _level;
}

int Entity::getX() {
    return x;
}

int Entity::getY() {
    return y;
}

int Entity::getLevel() {
    return level;
}