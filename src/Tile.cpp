//============================================================================
// Name        : Tile.cpp
// Author      : Robert Getka
// Version     : SE 2040, Spring 2016
//============================================================================
#include "Tile.h"

void Tile::notify(int distance) {
    if (distance == 0) {
        Application::printMessage(notifyMessage0);
    }
    if (distance == 1) {
        Application::printMessage(notifyMessage1);
    }
}
