//============================================================================
// Name        : Player.cpp
// Author      : Trevor Hacker
// Version     : SE 2040, Spring 2016
//============================================================================

#include "Player.h"
#include <string>
using namespace std;

Player::Player(){}

Player::Player(int _x, int _y, int _level) :Entity(_x, _y, _level) {}
char Player::getPrintChar() {
	return '+';
}

void Player::notify(int distance) {
	//does nothing
}