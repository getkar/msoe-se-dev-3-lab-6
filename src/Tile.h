//============================================================================
// Name        : Tile.h
// Author      : Robert Getka
// Version     : SE 2040, Spring 2016
//============================================================================
#ifndef LAB6_TILE_H
#define LAB6_TILE_H

#include "Application.h"


class Tile {
public:
    //notifies the tile that the player is the int distance away.
    virtual void notify(int);

    //returns the type of the tile
    virtual int getType() = 0;

    //returns the char used to represent the tile when printed.
    virtual char getPrintChar() = 0;

protected:
    // The method to be printed when notify(0) is called
    std::string notifyMessage0;
    // The method to be printed when notify(1) is called
    std::string notifyMessage1;
};

//A Tile with Bats
class BatTile : public Tile {
public:
    BatTile();

    int getType();

    char getPrintChar();
};

//An Empty Tile
class EmptyTile : public Tile {
public:
    EmptyTile();


     int getType();

     char getPrintChar();
};

// A Pit Tile
class PitTile : public Tile {
public:
    PitTile();


     int getType();

     char getPrintChar();
};

//A Stair Tile
class StairTile : public Tile {
public:
    StairTile();

     int getType();

     char getPrintChar();
};

#endif //LAB6_TILE_H
