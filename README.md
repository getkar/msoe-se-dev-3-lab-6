
```

Compile all source files, run. To access debug mode run with "d" as the command line argument.
	E.g. "lab6.exe d"


In the game there are 10 options available to the player accessed by typing the first letter into the console.

Arrow - fires an arrow in the direction specified, (an=north, as=south, ae=east, aw=west), anywhere from 1 to 3 rooms.
N)orth - moves the player North.
S)outh - moves the player South.
E)ast - moves the player East.
W)est - moves the player West.
U)p - only accessable if the player is standing on a staircase, moves the player up one level.
D)own - only accessable if the player is standing on a staircase, moves the player down one level.


Files

docs
	Rules.pdf
 	Rules.docx

src
	Application.ccp
 	Application.h
 	Arrow.cpp
 	Arrow.h
 	BatTile.cpp
 	EmptyTile.cpp
 	Entity.cpp
 	Entity.h
 	GameBoard.cpp
 	GameBoard.h
 	GameBoardGenerator.cpp
 	GameBoardGenerator.h
 	Monster.cpp
 	Mosnter.h
 	PitTile.cpp
 	Player.cpp
 	Player.h
 	StairTile.cpp
 	Tile.cpp
 	Tile.h   Note: Tile.h contains header information for all classes derived from Tile.
 	main.cpp
```